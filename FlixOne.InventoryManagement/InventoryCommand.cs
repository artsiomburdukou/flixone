﻿using System;

namespace FlixOne.InventoryManagement
{
    public abstract class InventoryCommand
    {
        private readonly bool _isTerminatingCommand;

        internal InventoryCommand(bool commandIsTerminating)
        {
            _isTerminatingCommand = commandIsTerminating;
        }

        public (bool wasSuccessful, bool shouldQuit) RunCommand()
        {
            return (InternalCommand(), _isTerminatingCommand);
        }

        internal abstract bool InternalCommand();
    }
}
