﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlixOne.InventoryManagement
{
    internal class HelpCommand : NonTerminatingCommand
    {
        internal override bool InternalCommand()
        {
            Console.WriteLine("HelpCommand");
            return true;
        }
    }
}
